import random

def readFile(path):
    content = []
    with open(path) as f:
        content = f.readlines()

    return [x.strip('\n') for x in content]

def makeTrie(words):
    trie = {}
    currentTrie = trie
    for word in words:
        for letter in word:
            if letter not in currentTrie:
                currentTrie[letter] = {}
            currentTrie = currentTrie[letter]
        currentTrie = trie

    return trie

def genWord(trie):
    currentTrie = trie
    word = ''
    while currentTrie.keys():
        letter = random.choice(currentTrie.keys())
        currentTrie = currentTrie[letter]
        word += letter
    return word

def genSentence(words, trie):
    sentence = ''
    word = 'temp'
    usedLetters = set()
    while len(usedLetters) < 26:
        word = genWord(trie)
        usedLetters |= set(list(word.lower()))
        sentence += word + " "
    return sentence

words = readFile('words.txt')
trie = makeTrie(words)

minLength = 999
minSentence = ''
while True:
    sentence = genSentence(words, trie)
    if len(sentence) <= minLength:
        minLength = len(sentence)
        minSentence = sentence
        print str(len(sentence)) + ': ' + sentence
